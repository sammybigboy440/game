package rest

import (
	"log"
	"net/http"

	utils "cesmanager/basic/pkgs/blocs/utilities"
	chess "cesmanager/custom/1/exec/game/pkgs/http/rest/chess"
	game "cesmanager/custom/1/exec/game/pkgs/http/rest/game"

	"github.com/gorilla/mux"
)

func init() {
	var need utils.Need
	need.ModID = 100
	need.Copy(&chess.Need)
	need.ModID = 100
	need.Copy(&game.Need)
}

//Main initializes Rest API routs
func Main() {
	module, err := utils.LoadModule(1, 100) //where 100 is the mod id
	if err != nil {
		log.Fatal(err.Error())
		return
	}
	r := mux.NewRouter()
	/**
	 * Game Routs
	 */
	r.HandleFunc("/create", game.Create)
	r.HandleFunc("/games", game.Games)
	r.HandleFunc("/inplay", game.InPlay)
	r.HandleFunc("/start", game.Start)
	r.HandleFunc("/stats", game.Start)
	/**
	 * Chess Routs
	 */
	r.HandleFunc("/chess/create", chess.Create)
	r.HandleFunc("/chess/join", chess.Join)
	r.HandleFunc("/chess/sync", chess.Sync)
	r.HandleFunc("/chess/play", chess.Play)

	//run server
	r.Host(module.Host)
	log.Fatal(http.ListenAndServe(":"+module.Port, r))
}
