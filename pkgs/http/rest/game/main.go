package rest

import (
	resp "cesmanager/basic/pkgs/blocs/response"
	utils "cesmanager/basic/pkgs/blocs/utilities"
	f "cesmanager/custom/1/exec/game/pkgs/blocs/game"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

//CoyID ...
var (
	Need utils.Need
)

func prep(r *http.Request) {
	Need.ID, _ = strconv.Atoi(strings.Join(r.Header["Custom-Coyid"], ""))
	Need.DBname = utils.Config.DataBase.DBPrefix + fmt.Sprintf("%d", Need.ID)
	Need.SubDomain = strings.Join(r.Header["Custom-Subdomain"], "")
	Need.Copy(&f.Need)
}

//Create ...
func Create(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	prep(r)
	//verify that this is a registered domain
	var resp resp.Response
	title := r.PostFormValue("title")
	code := r.PostFormValue("code")
	stake := r.PostFormValue("max_stake")
	active := r.PostFormValue("active")
	resp = f.Create(title, code, stake, active)
	json.NewEncoder(w).Encode(resp)
}

//Games ...
func Games(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	prep(r)
	if err := r.ParseForm(); err != nil {
		return
	}
	var posts []map[string]interface{}
	for k, v := range r.PostForm {
		var val interface{}
		val = v
		if k == "active" {
			val, _ = strconv.Atoi(strings.Join(v, ""))
		}
		posts = append(posts, map[string]interface{}{"q": k, "v": val})
	}

	json.NewEncoder(w).Encode(f.Fetch(posts))
}

//InPlay ...
func InPlay(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	prep(r)
	if err := r.ParseForm(); err != nil {
		return
	}
	var ses map[string]interface{}
	json.Unmarshal([]byte(strings.Join(r.Header["Custom-Session"], "")), &ses)
	var posts []map[string]interface{}
	for k, v := range r.PostForm {
		var val interface{}
		val = v
		if k == "game_id" {
			val, _ = strconv.Atoi(strings.Join(v, ""))
		}
		posts = append(posts, map[string]interface{}{"q": k, "v": val})
	}
	json.NewEncoder(w).Encode(f.InPlay(ses["id"], posts))
}

//Start ...
func Start(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	prep(r)
	//parse the form
	if err := r.ParseForm(); err != nil {
		return
	}

	playID := r.PostFormValue("play_id")
	json.NewEncoder(w).Encode(f.Start(playID))
}
