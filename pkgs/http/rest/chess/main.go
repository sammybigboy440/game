package chess

import (
	utils "cesmanager/basic/pkgs/blocs/utilities"
	f "cesmanager/custom/1/exec/game/pkgs/blocs/chess"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

//CoyID ...
var (
	Need utils.Need
)

func prep(r *http.Request) {
	Need.ID, _ = strconv.Atoi(strings.Join(r.Header["Custom-Coyid"], ""))
	Need.DBname = utils.Config.DataBase.DBPrefix + fmt.Sprintf("%d", Need.ID)
	Need.SubDomain = strings.Join(r.Header["Custom-Subdomain"], "")
	Need.Copy(&f.Need)
}

//Create ...
func Create(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	prep(r)
	//parse the form
	if err := r.ParseForm(); err != nil {
		return
	}
	gameID := r.PostFormValue("game_id")
	stake := r.PostFormValue("stake")
	var ses map[string]interface{}
	json.Unmarshal([]byte(strings.Join(r.Header["Custom-Session"], "")), &ses)
	json.NewEncoder(w).Encode(f.Create(gameID, stake, ses))
}

//Join ...
func Join(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	prep(r)

	//parse the form
	if err := r.ParseForm(); err != nil {
		return
	}

	var ses map[string]interface{}
	json.Unmarshal([]byte(strings.Join(r.Header["Custom-Session"], "")), &ses)

	gameID := r.PostFormValue("game_id")
	playID := r.PostFormValue("play_id")
	json.NewEncoder(w).Encode(f.Join(ses, gameID, playID))
}

//Sync ...
func Sync(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	prep(r)
	//parse the form
	if err := r.ParseForm(); err != nil {
		return
	}

	var ses map[string]interface{}
	json.Unmarshal([]byte(strings.Join(r.Header["Custom-Session"], "")), &ses)

	playID := r.PostFormValue("play_id")
	json.NewEncoder(w).Encode(f.Sync(playID, ses["id"]))
}

//Play ...
func Play(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	prep(r)
	//parse the form
	if err := r.ParseForm(); err != nil {
		return
	}

	var ses map[string]interface{}
	json.Unmarshal([]byte(strings.Join(r.Header["Custom-Session"], "")), &ses)
	playID := r.PostFormValue("play_id")
	move := r.PostFormValue("move")
	json.NewEncoder(w).Encode(f.Play(ses["id"], playID, move))
}
