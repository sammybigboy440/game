package connection

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	utils "cesmanager/basic/pkgs/blocs/utilities"

	"github.com/go-pg/pg"

	//for importing pg driver
	_ "github.com/lib/pq"
)

var (
	//Host ...
	Host = utils.Config.DataBase.Host
	//Port ...
	Port = utils.Config.DataBase.Port
	//User ..
	User = utils.Config.DataBase.User
	//Password ...
	Password = utils.Config.DataBase.Password
	//BaseDBName ...
	BaseDBName = utils.Config.DataBase.BaseDB
	//SslMode ...
	SslMode = utils.Config.DataBase.Ssl
	//debug Mode
	debug = utils.Config.DataBase.Debug
)

//DBname the default database
var DBname string

//DBParam struct
type DBParam struct {
	Host     string
	Port     string
	User     string
	Password string
	DBname   string
	Sslmode  string
}
type dbLogger struct{}

func (d dbLogger) BeforeQuery(q *pg.QueryEvent) {
	if !debug {
		return
	}
}

func (d dbLogger) AfterQuery(q *pg.QueryEvent) {
	if !debug {
		return
	}
	fmt.Println(q.FormattedQuery())
}

//PosgresConn used for POSTGRES DB connection
func (e *DBParam) PosgresConn() *pg.DB {
	opts := &pg.Options{
		User:     e.User,
		Password: e.Password,
		Addr:     e.Host + ":" + e.Port,
		Database: e.DBname,
	}
	db := pg.Connect(opts)
	if db == nil {
		log.Printf("Failed to connect to database.\n")
		os.Exit(100)
	}
	db.AddQueryHook(dbLogger{})
	return db
}

//DBConn connect to default database
func DBConn() *pg.DB {
	//connect with the database name injected from the importer
	return RDBConn(DBname)
}

//RDBConn connect to the specified database
func RDBConn(dbname string) *pg.DB {
	conn := DBParam{
		Host:     Host,
		Port:     Port,
		User:     User,
		Password: Password,
		DBname:   dbname,
		Sslmode:  SslMode,
	}
	db := conn.PosgresConn()
	return db
}

//getDBCoyConn connect to the specified database
func posgresConn() (*sql.DB, error) {
	conninfo := "user=" + User + " password=" + Password + " host=" + Host + " sslmode=" + SslMode
	db, err := sql.Open("postgres", conninfo)

	if err != nil {
		log.Printf("Unable to connect to Postgres, Reason: %v\n", err)
		return nil, err
	}
	return db, nil
}
