package postgres

import (
	utils "cesmanager/basic/pkgs/blocs/utilities"
	"cesmanager/custom/1/exec/game/internal/player"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

//Join create a copy of User struct
type Join struct {
	player.Join
	tableName struct{} `sql:"game_join,alias:game_join"`
	join      []string `sql:"-"`
	relation  []string `sql:"-"`
}

//Joins ...
func (e *Join) Joins(join string) *Join {
	e.join = append(e.join, join)
	return e
}

//Relation ...
func (e *Join) Relation(relation string) *Join {
	e.relation = append(e.relation, relation)
	return e
}

//Clear ...
func (e *Join) Clear(cl string) {
	switch cl {
	case "j":
	case "join":
		e.join = nil
		break
	case "r":
	case "relation":
		e.relation = nil
		break
	default:
		e.join = nil
		e.relation = nil
		break
	}
}

//Add save new user to the user table
func (e *Join) Add(db *pg.DB) (*Join, error) {
	//create the table
	e.CreateTable(db)

	// insert into the transaction table
	err := db.Insert(e)
	//update the balance in the Join
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//TAdd save new user to the user table
func (e *Join) TAdd(tx *pg.Tx) (*Join, error) {
	// insert into the transaction table
	err := tx.Insert(e)
	//update the balance in the Join
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Update the Join based on specified parameters
func (e *Join) Update(db *pg.DB, set []utils.SQLWhere, were []utils.SQLWhere) (*Join, error) {
	//create the table
	e.CreateTable(db)

	//begin a transaction
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	_, err = e.TUpdate(tx, set, were)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return e, err
	}
	tx.Commit()
	return e, nil
}

//TUpdate the Join the the specified parameters in a transaction
func (e *Join) TUpdate(tx *pg.Tx, set []utils.SQLWhere, were []utils.SQLWhere) (*Join, error) {
	//create the table
	qry := tx.Model(&e)
	//loop for set
	for i := 0; i < len(set); i++ {
		qry.Set(set[i].Query, set[i].Value)
	}
	for _, j := range e.join {
		qry.Join(j)
	}
	//loop for where
	for i := 0; i < len(were); i++ {
		qry.Where(were[i].Query, were[i].Value)
	}
	_, err := qry.Returning("*").Update()
	//Add the amount to the Joinlog
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Fetch based of param
func (e *Join) Fetch(db *pg.DB, param []utils.SQLWhere) ([]Join, error) {
	//create the table
	e.CreateTable(db)
	var wl []Join
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	wl, err = e.TFetch(tx, param)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return wl, err
	}
	tx.Commit()
	return wl, nil
}

//TFetch based of param
func (e *Join) TFetch(tx *pg.Tx, param []utils.SQLWhere) ([]Join, error) {
	var wl []Join
	qry := tx.Model(&wl)
	for i := 0; i < len(param); i++ {
		qry.Where(param[i].Query, param[i].Value)
	}
	for _, j := range e.relation {
		qry.Relation(j)
	}
	for _, j := range e.join {
		qry.Join(j)
	}
	err := qry.Select()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	return wl, nil
}

//CreateTable just as stated
func (e *Join) CreateTable(db *pg.DB) error {
	if tableExist(db, e) {
		return nil
	}
	var team Team
	err := team.CreateTable(db)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	opts := &orm.CreateTableOptions{
		IfNotExists:   true,
		FKConstraints: true,
	}
	err = db.CreateTable(e, opts)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}
