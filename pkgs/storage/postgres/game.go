package postgres

import (
	"context"
	"time"

	utils "cesmanager/basic/pkgs/blocs/utilities"
	m "cesmanager/custom/1/exec/game/internal/game"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

//Game create a copy of User struct
type Game struct {
	m.Game
	tableName  struct{} `sql:"game,alias:game"`
	TableExist bool     `sql:"-"`
}

//Add save new user to the user table
func (e *Game) Add(db *pg.DB) (*Game, error) {
	//create the table
	e.CreateTable(db)

	// insert into the transaction table
	err := db.Insert(e)
	//update the balance in the Game
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//TAdd save new user to the user table
func (e *Game) TAdd(tx *pg.Tx) (*Game, error) {
	// insert into the transaction table
	err := tx.Insert(e)
	//update the balance in the Game
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Update the Game based on specified parameters
func (e *Game) Update(db *pg.DB, set []utils.SQLWhere, were []utils.SQLWhere) (*Game, error) {
	//create the table
	e.CreateTable(db)

	//begin a transaction
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	_, err = e.TUpdate(tx, set, were)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return e, err
	}
	tx.Commit()
	return e, nil
}

//TUpdate the Game the the specified parameters in a transaction
func (e *Game) TUpdate(tx *pg.Tx, set []utils.SQLWhere, were []utils.SQLWhere) (*Game, error) {
	//create the table
	qry := tx.Model(&e)
	//loop for set
	for i := 0; i < len(set); i++ {
		qry.Set(set[i].Query, set[i].Value)
	}
	//loop for where
	for i := 0; i < len(were); i++ {
		qry.Where(were[i].Query, were[i].Value)
	}
	_, err := qry.Returning("*").Update()
	//Add the amount to the Gamelog
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Fetch based of param
func (e *Game) Fetch(db *pg.DB, param []utils.SQLWhere) ([]Game, error) {
	//create the table
	e.CreateTable(db)

	var wl []Game
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	wl, err = e.TFetch(tx, param)
	if err != nil {
		return wl, err
	}
	return wl, nil
}

//TFetch based of param
func (e *Game) TFetch(tx *pg.Tx, param []utils.SQLWhere) ([]Game, error) {
	var wl []Game
	qry := tx.Model(&wl)
	for i := 0; i < len(param); i++ {
		qry.Where(param[i].Query, param[i].Value)
	}
	err := qry.Select()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	return wl, nil
}

//BeforeInsert ...
func (e *Game) BeforeInsert(c context.Context, db orm.DB) error {
	if e.CreateDate.IsZero() {
		e.CreateDate = time.Now()
	}
	return nil
}

//CreateTable just as stated
func (e *Game) CreateTable(db *pg.DB) error {
	if tableExist(db, e) {
		return nil
	}
	opts := &orm.CreateTableOptions{
		IfNotExists:   true,
		FKConstraints: true,
	}
	err := db.CreateTable(e, opts)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}
