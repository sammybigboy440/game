package postgres

import (
	utils "cesmanager/basic/pkgs/blocs/utilities"
	m "cesmanager/custom/1/exec/game/internal/chess"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

//Chess create a copy of User struct
type Chess struct {
	m.Chess
	tableName  struct{} `sql:"game_chess,alias:game_chess"`
	TableExist bool     `sql:"-"`
	join       []string `sql:"-"`
	relation   []string `sql:"-"`
}

//Join ...
func (e *Chess) Join(join string) *Chess {
	e.join = append(e.join, join)
	return e
}

//Relation ...
func (e *Chess) Relation(relation string) *Chess {
	e.relation = append(e.relation, relation)
	return e
}

//Clear ...
func (e *Chess) Clear(cl string) {
	switch cl {
	case "j":
	case "join":
		e.join = nil
		break
	case "r":
	case "relation":
		e.relation = nil
		break
	}
}

//Add save new user to the user table
func (e *Chess) Add(db *pg.DB) (*Chess, error) {
	//create the table
	e.CreateTable(db)
	// insert into the transaction table
	err := db.Insert(e)
	//update the balance in the Chess
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//TAdd save new user to the user table
func (e *Chess) TAdd(tx *pg.Tx) (*Chess, error) {
	// insert into the transaction table
	err := tx.Insert(e)
	//update the balance in the Chess
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Update the Chess based on specified parameters
func (e *Chess) Update(db *pg.DB, set []utils.SQLWhere, were []utils.SQLWhere) (*Chess, error) {
	//create the table
	e.CreateTable(db)

	//begin a transaction
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	_, err = e.TUpdate(tx, set, were)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return e, err
	}
	tx.Commit()
	return e, nil
}

//TUpdate the Chess the the specified parameters in a transaction
func (e *Chess) TUpdate(tx *pg.Tx, set []utils.SQLWhere, were []utils.SQLWhere) (*Chess, error) {
	//create the table
	qry := tx.Model(e)
	//loop for set
	for i := 0; i < len(set); i++ {
		qry.Set(set[i].Query, set[i].Value)
	}
	//loop for where
	for i := 0; i < len(were); i++ {
		qry.Where(were[i].Query, were[i].Value)
	}
	_, err := qry.Returning("*").Update()
	//Add the amount to the Chesslog
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Fetch based of param
func (e *Chess) Fetch(db *pg.DB, param []utils.SQLWhere) ([]Chess, error) {
	//create the table
	e.CreateTable(db)

	var wl []Chess
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	wl, err = e.TFetch(tx, param)
	if err != nil {
		return wl, err
	}
	return wl, nil
}

//TFetch based of param
func (e *Chess) TFetch(tx *pg.Tx, param []utils.SQLWhere) ([]Chess, error) {
	var wl []Chess
	qry := tx.Model(&wl)
	for _, r := range e.relation {
		qry.Relation(r)
	}
	for i := 0; i < len(param); i++ {
		qry.Where(param[i].Query, param[i].Value)
	}
	err := qry.Select()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	return wl, nil
}

//CreateTable just as stated
func (e *Chess) CreateTable(db *pg.DB) error {
	if tableExist(db, e) {
		return nil
	}
	opts := &orm.CreateTableOptions{
		IfNotExists:   true,
		FKConstraints: true,
	}
	err := db.CreateTable(e, opts)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}
