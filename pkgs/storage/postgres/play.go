package postgres

import (
	"context"
	"time"

	utils "cesmanager/basic/pkgs/blocs/utilities"
	"cesmanager/custom/1/exec/game/internal/player"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

//Play create a copy of User struct
type Play struct {
	player.Play
	tableName  struct{} `sql:"game_play,alias:game_play"`
	TableExist bool     `sql:"-"`
	join       []string `sql:"-"`
	relation   []string `sql:"-"`
}

//Join ...
func (e *Play) Join(join string) *Play {
	e.join = append(e.join, join)
	return e
}

//Relation ...
func (e *Play) Relation(relation string) *Play {
	e.relation = append(e.relation, relation)
	return e
}

//Clear ...
func (e *Play) Clear(cl string) {
	switch cl {
	case "j":
	case "join":
		e.join = nil
		break
	case "r":
	case "relation":
		e.relation = nil
		break

	}
}

//Add save new user to the user table
func (e *Play) Add(db *pg.DB) error {
	//create the table
	e.CreateTable(db)

	// insert into the transaction table
	err := db.Insert(e)
	//update the balance in the Play
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}

//TAdd save new user to the user table
func (e *Play) TAdd(tx *pg.Tx) error {
	// insert into the transaction table
	err := tx.Insert(e)
	//update the balance in the Play
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}

//Update the Play based on specified parameters
func (e *Play) Update(db *pg.DB, set []utils.SQLWhere, were []utils.SQLWhere) error {
	//create the table
	e.CreateTable(db)

	//begin a transaction
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	err = e.TUpdate(tx, set, were)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return err
	}
	tx.Commit()
	return nil
}

//TUpdate the Play the the specified parameters in a transaction
func (e *Play) TUpdate(tx *pg.Tx, set []utils.SQLWhere, were []utils.SQLWhere) error {
	//create the table
	qry := tx.Model(e)
	//loop for set
	for i := 0; i < len(set); i++ {
		qry.Set(set[i].Query, set[i].Value)
	}
	//loop for where
	for i := 0; i < len(were); i++ {
		qry.Where(were[i].Query, were[i].Value)
	}
	_, err := qry.Returning("*").Update()
	//Add the amount to the Playlog
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}

//Fetch based of param
func (e *Play) Fetch(db *pg.DB, param []utils.SQLWhere) ([]Play, error) {
	//create the table
	e.CreateTable(db)
	var wl []Play
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	wl, err = e.TFetch(tx, param)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return wl, err
	}
	tx.Commit()
	return wl, nil
}

//TFetch based of param
func (e *Play) TFetch(tx *pg.Tx, param []utils.SQLWhere) ([]Play, error) {
	var wl []Play
	qry := tx.Model(&wl)
	for _, r := range e.relation {
		qry.Relation(r)
	}
	for _, j := range e.join {
		qry.Join(j)
	}
	for i := 0; i < len(param); i++ {
		qry.Where(param[i].Query, param[i].Value)
	}
	err := qry.Select()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	return wl, nil
}

//BeforeInsert ...
func (e *Play) BeforeInsert(c context.Context, db orm.DB) error {
	if e.CreateDate.IsZero() {
		e.CreateDate = time.Now()
	}
	return nil
}

//BeforeUpdate ...
func (e *Play) BeforeUpdate(c context.Context, db orm.DB) error {
	if e.CreateDate.IsZero() {
		e.ModifiedDate = time.Now()
	}
	return nil
}

//CreateTable just as stated
func (e *Play) CreateTable(db *pg.DB) error {
	if tableExist(db, e) {
		return nil
	}
	opts := &orm.CreateTableOptions{
		IfNotExists:   true,
		FKConstraints: true,
	}
	err := db.CreateTable(e, opts)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}
