package postgres

import (
	utils "cesmanager/basic/pkgs/blocs/utilities"
	"fmt"
	"reflect"
	"strings"

	"github.com/go-pg/pg"
)

//www.github.com/go-pg/pg/wiki/Writing-Queries
//DBname the default database
var (
	Need utils.Need
)

func tableExist(db *pg.DB, typ interface{}) bool {
	fd, ok := reflect.TypeOf(typ).Elem().FieldByName("tableName")
	if !ok {
		return false
	}
	q := fmt.Sprintf("SELECT 'public.%s'::regClass", strings.Split(strings.ReplaceAll(strings.Split(string(fd.Tag), ":")[1], "\"", ""), ",")[0])
	_, err := db.Exec(q)
	if err != nil {
		utils.LogError(err.Error(), 2)
		return false
	}
	return true
}
