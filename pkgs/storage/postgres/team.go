package postgres

import (
	utils "cesmanager/basic/pkgs/blocs/utilities"
	"cesmanager/custom/1/exec/game/internal/player"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

//Team create a copy of User struct
type Team struct {
	player.Team
	tableName  struct{} `sql:"game_team,alias:game_team"`
	TableExist bool     `sql:"-"`
	join       []string `sql:"-"`
	relation   []string `sql:"-"`
}

//Join ...
func (e *Team) Join(join string) *Team {
	e.join = append(e.join, join)
	return e
}

//Relation ...
func (e *Team) Relation(relation string) *Team {
	e.relation = append(e.relation, relation)
	return e
}

//Clear ...
func (e *Team) Clear(cl string) {
	switch cl {
	case "j":
	case "join":
		e.join = nil
		break
	case "r":
	case "relation":
		e.relation = nil
		break

	}
}

//Add save new user to the user table
func (e *Team) Add(db *pg.DB) (*Team, error) {
	//create the table
	e.CreateTable(db)

	// insert into the transaction table
	err := db.Insert(e)
	//update the balance in the Team
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//TAdd save new user to the user table
func (e *Team) TAdd(tx *pg.Tx) (*Team, error) {
	// insert into the transaction table
	err := tx.Insert(e)
	//update the balance in the Team
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Update the Team based on specified parameters
func (e *Team) Update(db *pg.DB, set []utils.SQLWhere, were []utils.SQLWhere) (*Team, error) {
	//create the table
	e.CreateTable(db)

	//begin a transaction
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	_, err = e.TUpdate(tx, set, were)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return e, err
	}
	tx.Commit()
	return e, nil
}

//TUpdate the Team the the specified parameters in a transaction
func (e *Team) TUpdate(tx *pg.Tx, set []utils.SQLWhere, were []utils.SQLWhere) (*Team, error) {
	//create the table
	qry := tx.Model(e)
	//loop for set
	for i := 0; i < len(set); i++ {
		qry.Set(set[i].Query, set[i].Value)
	}
	//loop for where
	for i := 0; i < len(were); i++ {
		qry.Where(were[i].Query, were[i].Value)
	}
	_, err := qry.Returning("*").Update()
	//Add the amount to the Teamlog
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Fetch based of param
func (e *Team) Fetch(db *pg.DB, param []utils.SQLWhere) ([]Team, error) {
	//create the table
	e.CreateTable(db)
	var wl []Team
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	wl, err = e.TFetch(tx, param)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return wl, err
	}
	tx.Commit()
	return wl, nil
}

//TFetch based of param
func (e *Team) TFetch(tx *pg.Tx, param []utils.SQLWhere) ([]Team, error) {
	var wl []Team
	qry := tx.Model(&wl)
	for _, r := range e.relation {
		qry.Relation(r)
	}
	for i := 0; i < len(param); i++ {
		qry.Where(param[i].Query, param[i].Value)
	}
	err := qry.Select()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	return wl, nil
}

//CreateTable just as stated
func (e *Team) CreateTable(db *pg.DB) error {
	if tableExist(db, e) {
		return nil
	}
	var play Play
	err := play.CreateTable(db)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	opts := &orm.CreateTableOptions{
		IfNotExists:   true,
		FKConstraints: true,
	}
	err = db.CreateTable(e, opts)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}
