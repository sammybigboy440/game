package postgres

import (
	utils "cesmanager/basic/pkgs/blocs/utilities"
	"cesmanager/custom/1/exec/game/internal/player"
	"strconv"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

//Player create a copy of User struct
type Player struct {
	tableName struct{} `sql:"game_player,alias:game_player"`
	player.Player
	join     []string `sql:"-"`
	relation []string `sql:"-"`
}

//Joins ...
func (e *Player) Joins(join string) *Player {
	e.join = append(e.join, join)
	return e
}

//Relation ...
func (e *Player) Relation(relation string) *Player {
	e.relation = append(e.relation, relation)
	return e
}

//Clear ...
func (e *Player) Clear(cl string) {
	switch cl {
	case "j":
	case "join":
		e.join = nil
		break
	case "r":
	case "relation":
		e.relation = nil
		break
	default:
		e.join = nil
		e.relation = nil
		break
	}
}

//Add save new user to the user table
func (e *Player) Add(db *pg.DB) error {
	//create the table
	e.CreateTable(db)

	// insert into the transaction table
	err := db.Insert(e)
	//update the balance in the Player
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}

//TAdd save new user to the user table
func (e *Player) TAdd(tx *pg.Tx) error {
	// insert into the transaction table
	err := tx.Insert(e)
	//update the balance in the Player
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return err
}

//Update the Player based on specified parameters
func (e *Player) Update(db *pg.DB, set []utils.SQLWhere, were []utils.SQLWhere) error {
	//create the table
	e.CreateTable(db)

	//begin a transaction
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	err = e.TUpdate(tx, set, were)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return err
	}
	tx.Commit()
	return nil
}

//TUpdate the Player the the specified parameters in a transaction
func (e *Player) TUpdate(tx *pg.Tx, set []utils.SQLWhere, were []utils.SQLWhere) error {
	//create the table
	qry := tx.Model(&e)
	//loop for set
	for i := 0; i < len(set); i++ {
		qry.Set(set[i].Query, set[i].Value)
	}
	//loop for where
	for i := 0; i < len(were); i++ {
		qry.Where(were[i].Query, were[i].Value)
	}
	_, err := qry.Returning("*").Update()
	//Add the amount to the Playerlog
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}

//Fetch based of param
func (e *Player) Fetch(db *pg.DB, param []utils.SQLWhere) ([]Player, error) {
	//create the table
	e.CreateTable(db)

	var wl []Player
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	wl, err = e.TFetch(tx, param)
	if err != nil {
		return wl, err
	}
	return wl, nil
}

//TFetch based of param
func (e *Player) TFetch(tx *pg.Tx, param []utils.SQLWhere) ([]Player, error) {
	var wl []Player
	qry := tx.Model(&wl)
	for i := 0; i < len(param); i++ {
		qry.Where(param[i].Query, param[i].Value)
	}
	err := qry.Select()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	return wl, nil
}

//Get - get a single player, if not exist create one
func (e *Player) Get(db *pg.DB, profileID string) error {
	e.CreateTable(db)
	e.Clear("a")
	//try get player
	e.Relation("Stat").Relation("Stat.Game").Relation("Profile")
	pls, err := e.Fetch(db, []utils.SQLWhere{
		{Query: "profile_id = ?", Value: profileID},
	})
	if err != nil {
		return err
	}
	if len(pls) == 0 {
		//create player
		e.ProfileID, err = strconv.Atoi(profileID)
		if err != nil {
			return err
		}
		err = e.Add(db)
		if err != nil {
			return err
		}
		err = e.Get(db, profileID)
		if err != nil {
			return err
		}
		return nil
	}
	*e = pls[0]
	return nil
}

//CreateTable just as stated
func (e *Player) CreateTable(db *pg.DB) error {
	if tableExist(db, e) {
		return nil
	}
	opts := &orm.CreateTableOptions{
		IfNotExists:   true,
		FKConstraints: true,
	}
	err := db.CreateTable(e, opts)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	var stat Stat
	err = stat.CreateTable(db)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	var team Team
	err = team.CreateTable(db)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}
