package postgres

import (
	utils "cesmanager/basic/pkgs/blocs/utilities"
	"cesmanager/custom/1/exec/game/internal/player"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

//Stat create a copy of User struct
type Stat struct {
	tableName struct{} `sql:"game_player_stat,alias:game_player_stat"`
	player.Stat
}

//Add save new user to the user table
func (e *Stat) Add(db *pg.DB) (*Stat, error) {
	//create the table
	e.CreateTable(db)

	// insert into the transaction table
	err := db.Insert(e)
	//update the balance in the Stat
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//TAdd save new user to the user table
func (e *Stat) TAdd(tx *pg.Tx) (*Stat, error) {
	// insert into the transaction table
	err := tx.Insert(e)
	//update the balance in the Stat
	if err != nil {
		utils.LogError(err.Error(), 1)
		return e, err
	}
	return e, nil
}

//Update the Stat based on specified parameters
func (e *Stat) Update(db *pg.DB, set []utils.SQLWhere, were []utils.SQLWhere) error {
	//create the table
	e.CreateTable(db)

	//begin a transaction
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	err = e.TUpdate(tx, set, were)
	if err != nil {
		utils.LogError(err.Error(), 1)
		tx.Rollback()
		tx.Commit()
		return err
	}
	tx.Commit()
	return nil
}

//TUpdate the Stat the the specified parameters in a transaction
func (e *Stat) TUpdate(tx *pg.Tx, set []utils.SQLWhere, were []utils.SQLWhere) error {
	//create the table
	qry := tx.Model(&e)
	//loop for set
	for i := 0; i < len(set); i++ {
		qry.Set(set[i].Query, set[i].Value)
	}
	//loop for where
	for i := 0; i < len(were); i++ {
		qry.Where(were[i].Query, were[i].Value)
	}
	_, err := qry.Returning("*").Update()
	//Add the amount to the Statlog
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}

//Fetch based of param
func (e *Stat) Fetch(db *pg.DB, param []utils.SQLWhere) ([]Stat, error) {
	//create the table
	e.CreateTable(db)

	var wl []Stat
	tx, err := db.Begin()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	wl, err = e.TFetch(tx, param)
	if err != nil {
		return wl, err
	}
	return wl, nil
}

//TFetch based of param
func (e *Stat) TFetch(tx *pg.Tx, param []utils.SQLWhere) ([]Stat, error) {
	var wl []Stat
	qry := tx.Model(&wl)
	for i := 0; i < len(param); i++ {
		qry.Where(param[i].Query, param[i].Value)
	}
	err := qry.Select()
	if err != nil {
		utils.LogError(err.Error(), 1)
		return wl, err
	}
	return wl, nil
}

//CreateTable just as stated
func (e *Stat) CreateTable(db *pg.DB) error {
	if tableExist(db, e) {
		return nil
	}
	opts := &orm.CreateTableOptions{
		IfNotExists:   true,
		FKConstraints: true,
	}
	err := db.CreateTable(e, opts)
	if err != nil {
		utils.LogError(err.Error(), 1)
		return err
	}
	return nil
}
