package game

import (
	"fmt"
	"net/url"
	"strconv"

	"github.com/gorilla/websocket"

	r "cesmanager/basic/pkgs/blocs/response"
	utils "cesmanager/basic/pkgs/blocs/utilities"
	c "cesmanager/custom/1/exec/game/pkgs/storage/connection"
	s "cesmanager/custom/1/exec/game/pkgs/storage/postgres"
)

//Need ..
var Need utils.Need

func prep() {
	Need.Copy(&s.Need)
	c.DBname = Need.DBname
	utils.Custom = Need.Custom
}

//Create - create a game
func Create(title string, code string, stake string, active string) r.Response {
	prep()
	var response r.Response
	var game s.Game
	var msgs []interface{}
	if len(title) < 1 {
		msg := "Title is required"
		msgs = append(msgs, map[string]string{"title": msg})
	}
	if len(msgs) > 0 {
		response.Body = msgs
		return response
	}
	db := c.DBConn()
	defer db.Close()
	game.Title = title
	game.Code = code
	game.MaxStake, _ = strconv.Atoi(stake)
	ac, _ := strconv.Atoi(active)
	game.Active = int8(ac)
	_, err := game.Add(db)
	if err != nil {
		msg := "Unable to create game"
		msgs = append(msgs, msg)
	} else {
		msg := "game Created"
		response.Status = true
		msgs = append(msgs, msg)
	}
	response.Body = msgs
	return response
}

//Update a game
func Update(id string, col []map[string]interface{}) r.Response {
	prep()
	var game s.Game
	var response r.Response
	var msgs []interface{}

	if len(id) < 1 {
		msg := "id is required"
		msgs = append(msgs, map[string]string{"user": msg})
	}
	if len(msgs) < 0 {
		var set []utils.SQLWhere
		var where []utils.SQLWhere
		for _, v := range col {

			if v["q"] == "id" {
				where = append(where, utils.SQLWhere{Query: fmt.Sprintf("%v = ?", v["q"]), Value: v["v"]})
			}
			set = append(where, utils.SQLWhere{Query: fmt.Sprintf("%v = ?", v["q"]), Value: v["v"]})
		}
		db := c.DBConn()
		defer db.Close()
		_, err := game.Update(db, set, where)
		if err != nil {
			msg := "Unable to update game"
			msgs = append(msgs, map[string]string{"user": msg})
		} else {
			response.Status = true
			msg := "game updated"
			msgs = append(msgs, map[string]string{"user": msg})
		}
	}
	response.Body = msgs
	return response
}

//Fetch available games
func Fetch(param []map[string]interface{}) r.Response {
	prep()
	var response r.Response
	var msgs []interface{}
	games, _ := InFetch(param)
	if len(games) < 1 {
		msgs = append(msgs, map[string]string{"game": "No game found"})
		response.Body = msgs
	} else {
		response.Status = true
		response.Body = games
	}
	return response
}

//InFetch ...
func InFetch(param []map[string]interface{}) ([]s.Game, error) {
	var game s.Game
	var where []utils.SQLWhere
	for _, v := range param {
		where = append(where, utils.SQLWhere{Query: fmt.Sprintf("%v = ?", v["q"]), Value: v["v"]})
	}
	db := c.DBConn()
	defer db.Close()

	return game.Fetch(db, where)
}

//InPlay - get available game play based on sepcified param
func InPlay(playerID interface{}, pm []map[string]interface{}) r.Response {
	prep()
	var response r.Response
	var msgs []interface{}

	db := c.DBConn()
	defer db.Close()
	var join s.Join
	join.Relation("Team").
		Relation("Team.Play").
		Relation("Team.Play.Starter").
		Relation("Team.Play.Team").
		Relation("Team.Play.Team.Joins").
		Relation("Team.Play.Team.Joins.Player").
		Relation("Team.Play.Team.Joins.Player.Profile")
	var param []utils.SQLWhere
	param = append(param, utils.SQLWhere{Query: "player_id = ?", Value: playerID})
	for _, v := range pm {
		param = append(param, utils.SQLWhere{Query: fmt.Sprintf("team__play.%v = ?", v["q"]), Value: v["v"]})
	}
	joins, err := join.Fetch(db, param)
	if err != nil {
		msgs = append(msgs, map[string]string{"error": "inplay error"})
		response.Body = msgs
		return response
	}
	join.Clear("a")
	var plays []interface{}
	for _, join := range joins {
		plays = append(plays, join.Team.Play)
	}

	response.Status = true
	response.Body = plays
	return response
}

//Start ...
func Start(playID string) r.Response {
	prep()
	var response r.Response
	var msgs []interface{}
	if len(playID) == 0 {
		msgs = append(msgs, map[string]string{"state": "play_id is required"})
		response.Body = msgs
		return response
	}
	db := c.DBConn()
	defer db.Close()
	var play s.Play
	were := []utils.SQLWhere{
		{Query: "id = ?", Value: playID},
	}
	//fetch the game to verify if itt has been populated
	plays, err := play.Fetch(db, were)
	if err != nil || len(plays) == 0 {
		msgs = append(msgs, map[string]string{"error": "invalid play"})
		response.Body = msgs
		return response
	}
	if plays[0].MinJoin > plays[0].Joined {
		msgs = append(msgs, map[string]string{"error": fmt.Sprintf("A minimum of %v player(s) are required", plays[0].MinJoin)})
		response.Body = msgs
		return response
	}
	set := []utils.SQLWhere{
		{Query: "Active = ?", Value: 1},
	}
	err = play.Update(db, set, were)
	if err != nil {
		msgs = append(msgs, map[string]string{"error": "unable to start"})
		response.Body = msgs
		return response
	}
	response.Status = true
	msgs = append(msgs, map[string]string{"success": "game started"})
	response.Body = msgs
	return response
}

//SocketMsg ...
func SocketMsg(dom string, msg string, mod string) error {
	host := dom + "." + utils.Config.Domain + ":" + utils.Config.Port
	u := url.URL{
		Scheme: "ws",
		Host:   host,
		Path:   "/api/socket",
	}
	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		return err
	}
	//tell the the socket the connection modID we will be communicating with
	err = c.WriteMessage(websocket.TextMessage, []byte(mod))
	if err != nil {
		return err
	}
	err = c.WriteMessage(websocket.TextMessage, []byte(msg))
	if err != nil {
		return err
	}
	err = c.Close()
	if err != nil {
		return err
	}
	return nil
}
