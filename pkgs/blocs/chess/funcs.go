package chess

import (
	utils "cesmanager/basic/pkgs/blocs/utilities"
	s "cesmanager/custom/1/exec/game/pkgs/storage/postgres"
	"errors"
	"strconv"

	"github.com/go-pg/pg"
)

func getChess(db *pg.DB, id int) (s.Chess, error) {
	var chess s.Chess
	tx, err := db.Begin()
	if err != nil {
		return chess, errors.New("get chess failed")
	}
	return tGetChess(tx, id)
}

func tGetChess(tx *pg.Tx, id int) (s.Chess, error) {
	var chess s.Chess
	chess.Relation("Play").
		Relation("White").
		Relation("White.Joins").
		Relation("White.Joins.Player").
		Relation("White.Joins.Player.Stat").
		Relation("White.Joins.Player.Profile").
		Relation("Black").
		Relation("Black.Joins").
		Relation("Black.Joins.Player").
		Relation("Black.Joins.Player.Stat").
		Relation("Black.Joins.Player.Profile")
	var param []utils.SQLWhere
	param = append(param, utils.SQLWhere{Query: "game_chess.play_id = ?", Value: id})
	lchess, err := chess.TFetch(tx, param)
	if err != nil || len(lchess) == 0 {
		return chess, errors.New("get chess failed")
	}
	if err != nil {
		return chess, err
	}
	return lchess[0], nil
}

//Join ...
func joiner(tx *pg.Tx, playerID int, gameID string, playID string) error {
	var join s.Join
	join.GameID, _ = strconv.Atoi(gameID)
	var param []utils.SQLWhere
	//get the play
	var play s.Play
	if len(playID) > 0 {
		param = nil
		param = append(param, utils.SQLWhere{Query: "game_play.id = ?", Value: playID})
		param = append(param, utils.SQLWhere{Query: "joined <= (team_count * team_limit)"})
		play.Relation("Game").Relation("Team").Relation("Starter")
	} else {
		param = append(param, utils.SQLWhere{Query: "game_id = ?", Value: gameID})
		param = append(param, utils.SQLWhere{Query: "joined <= (team_count * team_limit)"})
		param = append(param, utils.SQLWhere{Query: "starter_id IS NULL"})
	}
	plays, err := play.TFetch(tx, param)
	if err != nil || len(plays) == 0 {
		return errors.New("Game is unavailable")
	}
	play.ID = plays[0].ID
	//check if the user is already in the selected game
	join.Joins("INNER JOIN game_team AS t ON t.id = game_join.team_id")
	join.Joins("INNER JOIN game_play AS p ON p.id = t.play_id")
	join.Relation("Team").Relation("Team.Play").Relation("Game")
	param = nil
	param = append(param, utils.SQLWhere{Query: "player_id = ?", Value: playerID})
	param = append(param, utils.SQLWhere{Query: "p.id = ?", Value: play.ID})
	joins, err := join.TFetch(tx, param)
	if err != nil {
		return errors.New("unable to join")
	}
	if len(joins) > 0 {
		return nil
	}
	join.Clear("a")

	//get teams with space
	var team s.Team
	team.Relation("Play")
	param = nil
	param = append(param, utils.SQLWhere{Query: "play_id = ?", Value: play.ID})
	param = append(param, utils.SQLWhere{Query: "game_team.joined < game_team.limit"})
	teams, err := team.TFetch(tx, param)
	if err != nil || len(teams) == 0 {
		return errors.New("Game is unavailable")
	}
	team.Clear("r")
	//add the first open team
	team = teams[0]
	param = nil
	param = append(param, utils.SQLWhere{Query: "id = ?", Value: team.ID})
	var set []utils.SQLWhere
	set = append(set, utils.SQLWhere{Query: "joined = (joined + 1)"})
	_, err = team.TUpdate(tx, set, param)
	if err != nil || len(teams) == 0 {
		return errors.New("Team join failed")
	}
	//create the join
	join.TeamID = team.ID
	join.PlayerID = playerID
	_, err = join.TAdd(tx)
	if err != nil {
		return errors.New("unable to join")
	}
	//update the join counter
	param = nil
	param = append(param, utils.SQLWhere{Query: "id = ?", Value: play.ID})
	set = nil
	set = append(set, utils.SQLWhere{Query: "joined = joined + 1"})
	err = play.TUpdate(tx, set, param)
	if err != nil {
		tx.Rollback()
		tx.Commit()
		return errors.New("unable to join")
	}
	var stat s.Stat
	//fetch the player stats
	stats, err := stat.TFetch(tx, []utils.SQLWhere{
		{Query: "player_id = ?", Value: playerID},
	})
	if err != nil {
		tx.Rollback()
		tx.Commit()
		return errors.New("unable to join")
	}
	if len(stats) == 0 {
		//create it
		stat.PlayerID = playerID
		stat.GameID, _ = strconv.Atoi(gameID)
		stat.TAdd(tx)
		if err != nil {
			tx.Rollback()
			tx.Commit()
			return errors.New("unable to join")
		}

	}
	return nil
}
