package chess

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	r "cesmanager/basic/pkgs/blocs/response"
	utils "cesmanager/basic/pkgs/blocs/utilities"
	ichess "cesmanager/custom/1/exec/game/internal/chess"
	game "cesmanager/custom/1/exec/game/pkgs/blocs/game"
	c "cesmanager/custom/1/exec/game/pkgs/storage/connection"
	s "cesmanager/custom/1/exec/game/pkgs/storage/postgres"
)

//Need ...
var Need utils.Need

var player s.Player

func prep() {
	Need.Copy(&s.Need)
	c.DBname = Need.DBname
	utils.Custom = Need.Custom
}

//Create ...
func Create(gameID string, stake string, ses map[string]interface{}) r.Response {
	prep()
	var response r.Response
	var play s.Play
	var game s.Game
	var msgs []interface{}
	db := c.DBConn()

	defer db.Close()
	if len(gameID) == 0 {
		msgs = append(msgs, map[string]string{"game_id": "game_id is required"})
	} else {
		gms, err := game.Fetch(db, []utils.SQLWhere{
			{Query: "id = ?", Value: gameID},
		})
		if err != nil || len(gms) == 0 {
			msgs = append(msgs, map[string]string{"game_id": "unable to fild game"})
		}
		game = gms[0]
	}

	play.Stake, _ = strconv.Atoi(stake)
	if game.MaxStake < play.Stake {
		msgs = append(msgs, map[string]string{"game_id": "sorry you can not stake above " + fmt.Sprintf("%v", game.MaxStake)})
	}

	starter := fmt.Sprintf("%v", ses["id"])
	if len(starter) > 0 {
		err := player.Get(db, starter)
		if err != nil {
			msgs = append(msgs, map[string]string{"error": "unable to get game starter"})
		}
		play.StarterID = player.ID
	}
	if len(msgs) > 0 {
		response.Body = msgs
		return response
	}
	play.GameID, _ = strconv.Atoi(gameID)
	play.MinJoin = 2
	play.TeamCount = 2
	play.TeamLimit = 1

	err := play.CreateTable(db)
	if err != nil {
		msg := "Unable to start gameplay"
		msgs = append(msgs, map[string]string{"error": msg})
		response.Body = msgs
		return response
	}
	var chess s.Chess
	err = chess.CreateTable(db)
	if err != nil {
		msg := "Unable to create chess"
		msgs = append(msgs, map[string]string{"error": msg})
		response.Body = msgs
		return response
	}
	tx, err := db.Begin()
	if err != nil {
		msg := "Unable to create game"
		msgs = append(msgs, map[string]string{"error": msg})
		response.Body = msgs
		return response
	}
	err = play.TAdd(tx)
	if err != nil {
		tx.Rollback()
		tx.Commit()
		msg := "Unable to start gameplay"
		msgs = append(msgs, map[string]string{"error": msg})
		response.Body = msgs
		return response
	}

	//create record for the chess table
	chess.PlayID = play.ID
	fen := "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
	var board ichess.Board
	err = board.LoadFen(fen)
	if err != nil {
		tx.Rollback()
		tx.Commit()
		msg := "Unable to place piece"
		msgs = append(msgs, map[string]string{"error": msg})
		response.Body = msgs
		return response
	}
	chess.Board = board
	//create the teams
	for i := 0; i < int(play.TeamCount); i++ {
		var team s.Team
		team.PlayID = play.ID
		team.Limit = play.TeamLimit
		if i == 0 {
			team.Title = "White"
		} else {
			team.Title = "Black"
		}
		err = team.CreateTable(db)
		if err != nil {
			tx.Rollback()
			tx.Commit()
			response.Body = append(msgs, err.Error())
			return response
		}
		teamm, err := team.TAdd(tx)
		if err != nil {
			tx.Rollback()
			tx.Commit()
			response.Body = append(msgs, err.Error())
			return response
		}
		if i == 0 {
			chess.WhiteID = teamm.ID
		} else {
			chess.BlackID = teamm.ID
		}
	}
	schess, err := chess.TAdd(tx)
	if err != nil {
		tx.Rollback()
		tx.Commit()
		msg := "Unable to create chess"
		msgs = append(msgs, map[string]string{"error": msg})
		response.Body = msgs
		return response
	}
	if len(starter) > 0 {
		playerID, _ := strconv.Atoi(fmt.Sprintf("%v", ses["id"]))

		var join s.Join
		err = join.CreateTable(db)
		if err != nil {
			tx.Rollback()
			tx.Commit()
			msg := "Join initialization failed"
			msgs = append(msgs, map[string]string{"error": msg})
			response.Body = msgs
			return response
		}
		err = joiner(tx, playerID, gameID, fmt.Sprintf("%v", schess.PlayID))
		if err != nil {
			tx.Rollback()
			tx.Commit()
			msgs = append(msgs, map[string]string{"error": err.Error()})
			response.Body = msgs
			return response
		}
	}

	play.Relation("Starter").
		Relation("Team").
		Relation("Team.Joins").
		Relation("Team.Joins.Player").
		Relation("Team.Joins.Player.Profile").
		Relation("Team.Joins.Player.Stat")
	pls, err := play.TFetch(tx, []utils.SQLWhere{
		{Query: "game_play.id = ?", Value: play.ID},
	})
	if err != nil || len(pls) == 0 {
		tx.Rollback()
		tx.Commit()
		msg := "technical error, try again"
		msgs = append(msgs, map[string]string{"error": msg})
		response.Body = msgs
		return response
	}
	// schess.PlayID
	tx.Commit()

	response.Status = true
	response.Body = pls[0]
	return response
}

//Join ...
func Join(ses map[string]interface{}, gameID string, playID string) r.Response {
	prep()
	var response r.Response
	var msgs []interface{}
	if len(gameID) == 0 {
		msgs = append(msgs, map[string]string{"game_id": "game_id is required"})
	}
	db := c.DBConn()
	defer db.Close()
	profileID := fmt.Sprintf("%v", ses["id"])
	if len(profileID) > 0 {
		err := player.Get(db, profileID)
		if err != nil {
			msgs = append(msgs, map[string]string{"error": "unable to get game starter"})
		}
	}
	if len(msgs) > 0 {
		response.Body = msgs
		return response
	}
	tx, err := db.Begin()
	if err != nil {
		msg := "Unable to join game"
		response.Body = append(msgs, msg)
		return response
	}
	err = joiner(tx, player.ID, gameID, playID)
	if err != nil {
		tx.Rollback()
		tx.Commit()
		response.Body = append(msgs, err.Error())
		return response
	}
	//fetch full game
	pid, _ := strconv.Atoi(playID)
	chess, err := tGetChess(tx, pid)
	if err != nil {
		tx.Rollback()
		tx.Commit()
		msg := "unable to join"
		msgs = append(msgs, map[string]string{"error": msg})
		response.Body = msgs
		return response
	}
	tx.Commit()
	response.Status = true
	response.Body = chess
	return response
}

//Sync ..
func Sync(playID string, playerID interface{}) r.Response {
	prep()
	var response r.Response
	var msgs []interface{}
	db := c.DBConn()
	defer db.Close()
	if len(playID) == 0 {
		msgs = append(msgs, map[string]string{"play_id": "play_id is required"})
	}
	profileID := fmt.Sprintf("%v", playerID)
	if len(profileID) == 0 {
		msgs = append(msgs, map[string]string{"player_id": "player_id is required"})
	}
	if len(profileID) > 0 {
		err := player.Get(db, profileID)
		if err != nil {
			msgs = append(msgs, map[string]string{"error": "unable to get player"})
		}
	}
	if len(msgs) > 0 {
		response.Body = msgs
		return response
	}
	pid, _ := strconv.Atoi(playID)
	chess, err := getChess(db, pid)
	if err != nil {
		msgs = append(msgs, map[string]string{"error": "Sync failed."})
		response.Body = msgs
		return response
	}
	//push the game to socket
	rmsg := map[string]interface{}{
		"action": "load",
		"body":   chess,
	}
	msg, _ := json.Marshal(rmsg)
	err = game.SocketMsg(Need.SubDomain, string(msg), fmt.Sprintf("chess_%v", playID))
	if err != nil {
		msgs = append(msgs, map[string]string{"error": err.Error()})
		response.Body = msgs
		return response
	}

	response.Status = true
	chess.UserID, _ = strconv.Atoi(fmt.Sprintf("%v", playerID))
	response.Body = chess
	return response
}

//Play ...
func Play(playerID interface{}, playID string, move string) r.Response {
	prep()
	var response r.Response
	var msgs []interface{}
	db := c.DBConn()
	defer db.Close()

	if len(playID) == 0 {
		msgs = append(msgs, map[string]string{"play_id": "play_id is required"})
	}
	if len(move) == 0 {
		msgs = append(msgs, map[string]string{"move": "move is required"})
	}
	profileID := fmt.Sprintf("%v", playerID)
	if len(profileID) == 0 {
		msgs = append(msgs, map[string]string{"player_id": "player_id is required"})
	}
	if len(profileID) > 0 {
		err := player.Get(db, profileID)
		if err != nil {
			msgs = append(msgs, map[string]string{"error": "unable to get player"})
		}
	}
	if len(msgs) > 0 {
		response.Body = msgs
		return response
	}
	tx, err := db.Begin()
	if err != nil {
		msgs = append(msgs, map[string]string{"error": "unable to begin play"})
		response.Body = msgs
		return response
	}
	var chess s.Chess
	chess.Relation("Play")
	var param []utils.SQLWhere
	param = append(param, utils.SQLWhere{Query: "play_id = ?", Value: playID})
	pID, _ := strconv.Atoi(playID)
	chess, err = tGetChess(tx, pID)
	if err != nil || chess.PlayID == 0 {
		msgs = append(msgs, map[string]string{"error": "invalid game"})
		response.Body = msgs
		return response
	}
	chess.Clear("a")
	if chess.Play.Active == 0 {
		msgs = append(msgs, map[string]string{"error": "this game has not been started"})
		response.Body = msgs
		return response
	}
	if (player.ID == chess.White.Joins[0].PlayerID) != chess.Board.WhiteTurn {
		msgs = append(msgs, map[string]string{"error": "its not your turn to play"})
		response.Body = msgs
		return response
	}
	history := chess.Load()
	if move == history[len(history)-1] {
		msgs = append(msgs, map[string]string{"error": "no move made"})
		response.Body = msgs
		return response
	}
	moves := strings.Split(move, "[*]")
	if len(moves) > 1 {
		err = chess.Move(moves[0], false)
		err = chess.Move(moves[1], true)
	} else {
		err = chess.Move(move, true)
	}

	board, err1 := json.Marshal(chess.Board)
	if err != nil || err1 != nil {
		msgs = append(msgs, map[string]string{"error": "unable to move piece"})
		response.Body = msgs
		return response
	}
	set := []utils.SQLWhere{
		{Query: "board = ?", Value: string(board)},
	}
	_, err = chess.TUpdate(tx, set, param)
	if err != nil {
		msgs = append(msgs, map[string]string{"error": "play failed"})
		response.Body = msgs
		return response
	}

	chess.Load()
	chess.UserID, _ = strconv.Atoi(fmt.Sprintf("%v", playerID))
	response.Body = chess
	rmsg := map[string]interface{}{
		"action": "move",
		"body":   moves,
	}
	msg, err := json.Marshal(rmsg)
	//push to socket
	err = game.SocketMsg(Need.SubDomain, string(msg), "chess")
	if err != nil {
		msgs = append(msgs, map[string]string{"error": err.Error()})
		response.Body = msgs
		tx.Rollback()
		tx.Commit()
		return response
	}
	pid, _ := strconv.Atoi(playID)
	chess, err = getChess(db, pid)
	if err != nil {
		msgs = append(msgs, map[string]string{"error": "unable to fetch game"})
		response.Body = msgs
		tx.Rollback()
		tx.Commit()
		return response
	}
	response.Status = true
	tx.Commit()
	return response
}
