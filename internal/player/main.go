package player

import (
	"cesmanager/basic/export/profile"
	"cesmanager/custom/1/exec/game/internal/game"
	"time"
)

//Profile ...
type Profile struct {
	tableName struct{} `sql:"profile"`
	profile.Profile
}

//Player ...
type Player struct {
	tableName struct{} `sql:"game_player"`
	ID        int
	ProfileID int
	Profile   *Profile
	Stat      []Stat
}

//Stat ...
type Stat struct {
	tableName struct{}   `sql:"game_player_stat"`
	PlayerID  int        `sql:",notnull"`
	Player    *Player    //
	GameID    int        `sql:",notnull"`
	Game      *game.Game //
	Won       int        `sql:",use_zero,default:0"`
	Lost      int        `sql:",use_zero,default:0"`
	Draw      int        `sql:",use_zero,default:0"`
	Rate      int        `sql:",use_zero,default:0"`
}

//Play ...
type Play struct {
	tableName    struct{}   `sql:"game_play"`
	ID           int        `sql:",alias:play_id,pk"`
	GameID       int        `sql:"game_id,notnull"`
	Game         *game.Game //
	StarterID    int        //if its null then the game was a free to any game
	Starter      *Player    //
	Team         []*Team    //
	Joined       int        `sql:",use_zero,default:0"` //total joins
	MinJoin      int        `sql:",use_zero,notnull"`   //minimum number of people required before start
	Winner       int        //team that won
	Active       int8       `sql:",use_zero,default:0"`
	TeamCount    int8       `sql:",use_zero,default:0"`
	TeamLimit    int8       `sql:",use_zero,default:0"`
	Stake        int        `sql:",use_zero,default:0"` //total stake
	CreateDate   time.Time  `sql:"started,default:now()"`
	ModifiedDate time.Time  `sql:"ended"`
}

//Team ...
type Team struct {
	tableName struct{} `sql:"game_team"`
	ID        int      `sql:",alias:team_id,pk"`
	Title     string   `sql:"type:varchar(10)"`
	PlayID    int      `sql:"play_id,notnull"`
	Play      *Play    //
	Joined    int      `sql:",use_zero,default:0"` //count of people that have joined
	Limit     int8
	Joins     []*Join //
}

//Join ...
type Join struct {
	tableName struct{}   `sql:"game_join"`
	GameID    int        //
	Game      *game.Game //
	TeamID    int        `sql:"team_id,unique:player"`
	Team      Team       //
	PlayerID  int        `sql:",unique:player"`
	Player    *Player    //
	Stake     int        `sql:"amount,use_zero"`
}
