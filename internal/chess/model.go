package chess

import (
	"cesmanager/custom/1/exec/game/internal/player"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

//HistorySep ..
var HistorySep = "[**]"

//Chess ...
type Chess struct {
	tableName  struct{}     `sql:"game_chess,alias:game_chess"`
	PlayID     int          `sql:",notnull"`
	Play       *player.Play //
	Selected   string       `sql:",type:varchar(3)"`
	LastMove   string       `sql:",type:varchar(7)"`
	WhiteID    int          `sql:",notnull"`
	White      *player.Team //
	BlackID    int          `sql:",notnull"`
	Black      *player.Team //
	Board      Board        //
	History    string       //
	HistoryLog []string     `sql:"-"`
	UserID     int          `sql:"-"`
}

//Load ...
func (e *Chess) Load() []string {
	e.HistoryLog = strings.Split(e.History, HistorySep)
	return e.HistoryLog
}

//Move ...
func (e *Chess) Move(move string, turnSwap bool) error {
	rmv := strings.Split(move, "|")
	mv1 := strings.Split(rmv[0], ",")
	x1, err := strconv.Atoi(mv1[0])
	if err != nil {
		return err
	}
	z1, err := strconv.Atoi(mv1[1])
	if err != nil {
		return err
	}
	mv2 := strings.Split(rmv[1], ",")
	x2, err := strconv.Atoi(mv2[0])
	if err != nil {
		return err
	}
	z2, err := strconv.Atoi(mv2[0])
	if err != nil {
		return err
	}
	e.Board.move(x1, z1, x2, z2, turnSwap)
	return nil
}

//Promote ...
func (e *Chess) Promote(kind string, x int, z int) error {
	err := e.Board.promote(kind, x, z)
	if err != nil {
		return err
	}
	return nil
}

//Bond ...
func (e *Chess) Bond(history []string, add string) string {
	history = append(history, add)
	return strings.Join(history, HistorySep)
}

//Board ...
type Board struct {
	Position  [8][8]Piece
	Capture   []Piece
	WhiteTurn bool
	Castles   map[string]bool
	Enpasant  string
}

//Piece ...
type Piece struct {
	ID   string
	Kind string
}

var wpcs string = "rnbqkbnrp"
var bpcs string = "RNBQKBNRP"

//LoadFen ...
func (e *Board) LoadFen(fen string) error {
	alph := "abcdefg"
	rs := strings.Split(fen, "/")
	e.Castles = map[string]bool{}
	for i := 0; i < len(rs); i++ {
		if i == 7 {
			pts := strings.Split(rs[i], " ")
			e.loadFenRow(pts[0], i)
			e.WhiteTurn = pts[1] == "w"
			//load castle
			castles := strings.Split(pts[2], "")
			for _, ct := range castles {
				e.Castles[ct] = true
			}

			emp := strings.Split(pts[3], " ")
			if len(emp) > 1 {
				e.Enpasant = fmt.Sprintf("%v|%v", strings.Index(alph, emp[0]), emp[1])
			}
			break
		}
		err := e.loadFenRow(rs[i], i)
		if err != nil {
			return err
		}
	}
	return nil
}

func (e *Board) move(x1 int, z1 int, x2 int, z2 int, turnSwap bool) error {
	iw1 := strings.ContainsAny(wpcs, e.Position[z1][x1].Kind)
	iw2 := strings.ContainsAny(wpcs, e.Position[z2][x2].Kind)
	if iw1 == iw2 {
		return errors.New("Invalid move")
	}

	if e.Position[z1][x1].Kind == "" {
		return errors.New("Invalid move")
	}
	if e.Position[z2][x2].Kind != "" {
		return errors.New("Invalid move")
	}
	e.Position[z2][x2] = e.Position[z1][x1]
	e.Position[z1][x1] = Piece{}
	if turnSwap {
		e.WhiteTurn = !e.WhiteTurn
	}
	return nil
}
func (e *Board) loadFenRow(row string, i int) error {
	pcs := strings.Split(row, "")
	for j := 0; j < len(pcs); j++ {
		if !strings.Contains(wpcs+bpcs, pcs[j]) {
			blanks, err := strconv.Atoi(pcs[j])
			if err != nil {
				return err
			}
			cnt := blanks + j
			for b := j; b < cnt; b++ {
				e.Position[i][b] = Piece{}
				j = b
			}
			continue
		}
		id := ""
		keyFound := true
		for keyFound {
			id = fmt.Sprintf("pc%v", rand.Int())
			keyFound = e.IDExist(id)
		}
		e.Position[i][j] = Piece{
			ID:   id,
			Kind: fmt.Sprintf("%s", pcs[j]),
		}
	}
	return nil
}

func (e *Board) promote(kind string, x int, z int) error {
	isWhite := strings.Contains(wpcs, e.Position[x][z].Kind)
	if isWhite && z != 7 {
		return errors.New("Invalid move")
	}
	if !isWhite && z != 0 {
		return errors.New("Invalid move")
	}
	if e.Position[x][z].Kind == "" {
		return errors.New("Invalid move")
	}
	e.Position[x][z] = Piece{
		ID:   e.Position[x][z].ID,
		Kind: kind,
	}
	return nil
}

//IDExist ...
func (e *Board) IDExist(id string) bool {
	for i := 0; i < 8; i++ {
		for j := 0; j < 8; j++ {
			if e.Position[i][j].Kind == "" {
				continue
			}
			if e.Position[i][j].ID == id {
				return true
			}
		}
	}
	return false
}
