package game

import (
	"time"
)

//Game ...
type Game struct {
	tableName  struct{}  `sql:"game"`
	ID         int       `sql:",alias:game_id,pk"`
	Code       string    `sql:"code,notnull"`
	Title      string    `sql:"title"`
	Modes      string    `sql:"modes"` //comma seperated allowed game mode
	MaxStake   int       `sql:",default:0"`
	Active     int8      `sql:"active,default:0"`
	CreateDate time.Time `sql:"create_date"`
}
