package main

import (
	"fmt"

	h "cesmanager/custom/1/exec/game/pkgs/http/rest"
)

func main() {
	fmt.Println("Initializing via Http protocal")
	h.Main()
}
